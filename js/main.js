
$(function() {
    calculateSidePatterWidth();

    window.sr = new scrollReveal();

    $('.team').flickity({
      // options
      cellAlign: 'left',
      contain: true,
      autoPlay: 3000,
      wrapAround: true,
      prevNextButtons: false
    });

    // initialize vars for code to hide and reveal header
    var lastScrollPos = 0;

    // initialize vars for parallax effect in #hero
    var masthead = document.getElementById('masthead');

    $(window).scroll(function () {

        //header scroll code
        var currentScrollPos = $(this).scrollTop();
        if (currentScrollPos > lastScrollPos) {
            $('header').addClass('away');
        } else {
           $('header').removeClass('away');
        }
        lastScrollPos = currentScrollPos;

        //parallax code
        var shift = getScrollPercent(80);
        var transform = shift * 8;
        masthead.style.webkitTransform = "translateY(-" + transform + "px)";
        masthead.style.transform = "translateY(-" + transform + "px)";
        masthead.style.opacity = 1 - getScrollPercent(5);
    });


    //js for smooth scrolling
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });
});

function calculateSidePatterWidth(){
    var screenWidth = $(window).width();
    var containerWidth = $('.lines').width();
    var sideWidth = (screenWidth - containerWidth) / 2;
    var oneColumn = containerWidth / 12;
    console.log('screenWidth: ' + screenWidth + '\ncontainerWidth: ' + containerWidth + '\nsideWidth: ' + sideWidth);
    $('.side-pattern').width(sideWidth + oneColumn);
} 


function getScrollPercent(val) {
    var h = document.documentElement,
        b = document.body,
        st = 'scrollTop',
        sh = 'scrollHeight';
    return h[st] || b[st] / ((h[sh] || b[sh]) - h.clientHeight) * val;
}




    
